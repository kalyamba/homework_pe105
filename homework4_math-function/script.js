let num1 = prompt('Введіть перше число:');
let num2 = prompt('Введіть друге число:');
let operation = prompt('Введіть математичну операцію (+, -, *, /):');

function calculate(num1, num2, operation) {
    switch (operation) {
        case '+':
            return num1 + num2;
        case '-':
            return num1 - num2;
        case '*':
            return num1 * num2;
        case '/':
            return num1 / num2;

    }
}

num1 = Number(num1);
num2 = Number(num2);

const result = calculate(num1, num2, operation);
console.log('Результат виконання операції:', result);
