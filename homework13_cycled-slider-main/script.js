// #Теоретический вопрос

// 1. Опишите своими словами разницу между функциями `setTimeout()` и `setInterval()`.
// 2. Что произойдет, если в функцию `setTimeout()` передать нулевую задержку? Сработает ли она мгновенно, и почему?
// 3. Почему важно не забывать вызывать функцию `clearInterval()`, когда ранее созданный цикл запуска вам уже не нужен?

// 1. Функция setTimeout() позволяет запустить код один раз через заданый промежуток времени, а 
// функция setInterval() позволяет запускать код регулярно через заданый промежуток времени.
// 2. Она сработает, но сразу же будет накапливаться "погрешность" и в итоге не будет четкого,
// мгновенного выполнения.
// 3. clearInterval() позволяет остановит ьвыполнение функции setInterval(). Это нужно для того,
// что бы мы не получили бесконечное выполнение функции. Сделать это можно записав индификатор нашего setInterval()
// в переменную (variable = setInterval(...)), и вызвав после функции setInterval() , функцию clearInterval(variable)

let images = document.querySelectorAll(".image-to-show");
let stopBtn = document.querySelector("#stop-action");
let resumeBtn = document.querySelector("#resume-action");
let imgWrap = document.querySelector(".images-wrapper");
let timerElem = document.querySelector("#timer");
let intervalId = null;
let timerId = null;
let index = 3;

function countDown () {
  let startDate = Date.now();
  let endDate = startDate + 3000;
  timerId = setInterval(() => {
    let now = Date.now();
    let remainingTime = endDate - now;
    if (remainingTime <= 0) {
      clearInterval(timerId);
      remainingTime = 0
    }
    timerElem.innerText = `${remainingTime / 1000}`;
  }, 100);
}

function showImage() {

  intervalId = setTimeout(function swapActive() {
    countDown();
    images[index].classList.remove("active");
    index++;
    if (index >= images.length) {
      index = 0;
    }
    images[index].classList.add("active");
    intervalId = setTimeout(swapActive, 3000);
  }, 3000);
}


window.addEventListener("load", function () {
  countDown();
  showImage();
  
});

stopBtn.addEventListener('click', function(){
  clearInterval(timerId);
  timerId = null;
  clearTimeout(intervalId);
})

resumeBtn.addEventListener("click", function(){
  countDown();
  showImage();
})

