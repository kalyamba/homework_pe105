const passwordInput = document.querySelectorAll('input[type="password"]')[0];
const confirmPasswordInput = document.querySelectorAll('input[type="password"]')[1];
const passwordIcons = document.querySelectorAll('.icon-password');
let errorMessageElement = null;

function togglePasswordVisibility(event) {
    const icon = event.target;
    const input = icon.previousElementSibling;

    const isVisible = input.getAttribute('type') === 'text';

    input.setAttribute('type', isVisible ? 'password' : 'text');
    icon.classList.toggle('fa-eye', !isVisible);
    icon.classList.toggle('fa-eye-slash', isVisible);
}

for (let i = 0; i < passwordIcons.length; i++) {
    passwordIcons[i].addEventListener('click', togglePasswordVisibility);
}

document.querySelector('.password-form').addEventListener('submit', function (event) {
    event.preventDefault();

    const passwordValue = passwordInput.value;
    const confirmPasswordValue = confirmPasswordInput.value;

    if (passwordValue === confirmPasswordValue) {
        if (errorMessageElement) {
            errorMessageElement.parentNode.removeChild(errorMessageElement);
            errorMessageElement = null;
        }
        alert('You are welcome');
    } else {
        if (!errorMessageElement) {
            errorMessageElement = document.createElement('span');
            errorMessageElement.textContent = 'Нужно ввести одинаковые значения';
            errorMessageElement.style.color = 'red';
            confirmPasswordInput.parentNode.appendChild(errorMessageElement);
        }
    }
});

passwordInput.addEventListener('input', function () {
    if (errorMessageElement) {
        errorMessageElement.parentNode.removeChild(errorMessageElement);
        errorMessageElement = null;
    }
});

confirmPasswordInput.addEventListener('input', function () {
    if (errorMessageElement) {
        errorMessageElement.parentNode.removeChild(errorMessageElement);
        errorMessageElement = null;
    }
});


