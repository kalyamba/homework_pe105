const findMultiplesOfFive = () => {
    const userInput = parseInt(prompt("Введіть число:"));
    if (isNaN(userInput)) {
        console.log("Ви ввели недійсне число. Будь ласка, спробуйте ще раз.");
        return;
    }

    let multiplesOfFive = [];
    for (let i = 0; i <= userInput; i++) {
        if (i % 5 === 0) {
            multiplesOfFive.push(i);
        }
    }

    if (multiplesOfFive.length === 0) {
        console.log("Sorry, no numbers.");
    } else {
        console.log("Числа кратні 5 від 0 до " + userInput + ":");
        console.log(multiplesOfFive);
    }
};

findMultiplesOfFive();
