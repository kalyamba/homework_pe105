const passwordInput = document.querySelectorAll('input[type="password"]')[0];
const confirmPasswordInput = document.querySelectorAll('input[type="password"]')[1];
const passwordIcons = document.querySelectorAll('.icon-password');

function togglePasswordVisibility(event) {
    const icon = event.target;
    const input = icon.previousElementSibling;

    const isVisible = input.getAttribute('type') === 'text';

    input.setAttribute('type', isVisible ? 'password' : 'text');
    icon.classList.toggle('fa-eye', !isVisible);
    icon.classList.toggle('fa-eye-slash', isVisible);
}

for (let i = 0; i < passwordIcons.length; i++) {
    passwordIcons[i].addEventListener('click', togglePasswordVisibility);
}

document.querySelector('.password-form').addEventListener('submit', function (event) {
    event.preventDefault();

    const passwordValue = passwordInput.value;
    const confirmPasswordValue = confirmPasswordInput.value;

    if (passwordValue === confirmPasswordValue) {
        alert('You are welcome');
    } else {
        const errorMessage = document.createElement('span');
        errorMessage.textContent = 'Нужно ввести одинаковые значения';
        errorMessage.style.color = 'red';
        confirmPasswordInput.parentNode.appendChild(errorMessage);
    }
});

function changeTheme() {
    const body = document.getElementsByTagName("body")[0];
    body.classList.toggle("dark-theme");

    const isDarkTheme = body.classList.contains("dark-theme");
    localStorage.setItem("isDarkTheme", isDarkTheme);
}

window.addEventListener("DOMContentLoaded", () => {
    const isDarkTheme = localStorage.getItem("isDarkTheme");
    const body = document.getElementsByTagName("body")[0];

    if (isDarkTheme === "true") {
        body.classList.add("dark-theme");
    }
});